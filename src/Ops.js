export const defineCompareOp = (
  binOp,
  nullaryDefault=true,
  unaryDefault=true
) => (...args) => {
  if (args.length < 1)
    return nullaryDefault
  if (args.length < 2)
    return unaryDefault

  let prev = null
  for (const arg of args) {
    if (prev != null && ! (prev < arg))
      return false
    prev = arg
  }
  return true
}

// ----------------------------------------------------
// Comparison
// ----------------------------------------------------
export const binLt = (a, b) => (a < b)
export const lt = defineCompareOp(binLt)

export const binLe = (a, b) => (a <= b)
export const le = defineCompareOp(binLt)

export const binEq = (a, b) => (a == b)
export const eq = defineCompareOp(binEq)

export const binEqq = (a, b) => (a === b)
export const eqq = defineCompareOp(binEqq)

export const binNe = (a, b) => (a != b)
export const ne = defineCompareOp(binNe)

export const binNne = (a, b) => (a !== b)
export const nne = defineCompareOp(binNne)

export const binGt = (a, b) => (a > b)
export const gt = defineCompareOp(binGt)

export const binGe = (a, b) => (a >= b)
export const ge = defineCompareOp(binGe)
// ----------------------------------------------------


// ----------------------------------------------------
// Bitwise
// ----------------------------------------------------
export const binBitOr = (a, b) => (a | b)
// export const bitOr = defineCompareOp(binBitOr)

export const binBitAnd = (a, b) => (a & b)
// export const bitAnd = defineCompareOp(binBitAnd)

export const binBitXor = (a, b) => (a ^ b)
// export const bitXor = defineCompareOp(binBitXor)
// // ----------------------------------------------------

// ----------------------------------------------------
// Logical
// ----------------------------------------------------
export const binOr = (a, b) => (a || b)
export const or = defineCompareOp(binOr)

export const binAnd = (a, b) => (a && b)
export const and = defineCompareOp(binAnd)
// ----------------------------------------------------



export default {
  defineCompareOp,
  binLt, lt,
  binLe, le,
  binEq, eq,
  binEqq, eqq,
  binNe, ne,
  binNne, nne,
  binGt, gt,
  binGe, ge,
  binOr, or,
  binAnd, and,
  binBitOr, // bitOr,
  binBitAnd, // bitAnd,
  binBitXor, // bitXor,
}
