import Sampler from '#src/Random/Sampler'
import { camelCase } from 'change-case'

if (typeof window === 'undefined') {
  let fetch
  import('node-fetch')
    .then((res) => {
      fetch = res
    })
}

const PREFIX = 'https://bvraghav.com/d'
const ADJ = 'adj.list'
const NAMES = 'architect-names.list'

let _adj_, _names_

export async function fetchAdj() {
  if (! _adj_) {
    _adj_ = await fetchAndSplitByLines(`${PREFIX}/${ADJ}`)
  }
  return _adj_
}

export async function fetchNames() {
  if (! _names_) {
    _names_ = await fetchAndSplitByLines(`${PREFIX}/${NAMES}`)
  }
  return _names_
}

export async function fetchAndSplitByLines(uri) {
  let res
  res = await fetch(uri)
  res = await res.text()
  res = res.split('\n')
  res = res.filter((s) => !!s)
  res = res.map(camelCase)
  return res
}

export async function twoWordNames(N=1, seed) {
  const sampler = new Sampler(seed)
  const adj = sampler.sample(await fetchAdj(), {k:N})
  const names = sampler.sample(await fetchNames(), {k:N})

  return [...Array(N).keys()].map(
    (i) => `${adj[i]}_${names[i]}`
  )
}

export async function twoWordName(seed) {
  return (await twoWordNames(1, seed)).pop()
}

