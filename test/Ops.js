import {describe, test, it, expect} from 'vitest'

import {lt} from '#src/Ops'

// const { Symbol } = await import ('#src/Symbol.js')

describe('Std Less Operator', () => {
  it('Nullary is always true', () => {
    expect(lt()).toBeTruthy()
  })
  it('Unary is always true', () => {
    expect(lt(1)).toBeTruthy()
  })
  it('Binary strictly less.', () => {
    expect(lt(1,2)).toBeTruthy()
    expect(lt(1,1)).toBeFalsy()
    expect(lt(1,0)).toBeFalsy()
  })
  it('N-ary strictly less.', () => {
    expect(lt(1,2,3)).toBeTruthy()
    expect(lt(0,1,1)).toBeFalsy()
    expect(lt(0,1,0)).toBeFalsy()
  })
})
