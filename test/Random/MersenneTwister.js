import {describe, test, it, expect} from 'vitest'
import {MersenneTwister} from '#src/Random/MersenneTwister'

const seed = 4644286892836
const stream = (sampler) =>
      [...Array(100000)].map(() => sampler.random())

describe('MersenneTwister', () => {

  it('MT in range [0,1]', () => {
    const s = stream(new MersenneTwister(seed))
    expect(
      s.every((x) => (0 <= x && x <= 1))
    ).toBeTruthy()
  })

  it('MT stream is reproducible by seed', () => {
    const s1 = stream(new MersenneTwister(seed))
    const s2 = stream(new MersenneTwister(seed))
    expect(s1).toEqual(s2)
  })

  it('MT stream is uniform', () => {
    const E = (seq) => (seq.reduce((s, x) => (s+x))
                        / seq.length)
    const sq_ = (x) => (x*x)
    const sq = (seq) => seq.map(sq_)
    const X = stream(new MersenneTwister(seed))
    const meanX = E(X)
    const varX = E(sq(X)) - sq_(meanX)
    expect(meanX).toBeCloseTo(1/2)
    expect(varX).toBeCloseTo(1/12)
  })
  
})

