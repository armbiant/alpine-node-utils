// ----------------------------------------------------
// This js is a test whether (a + b) >> 1 == (a ^ b)
// Which it obviously is not. But also an example of
// how to use the powers of ViTest in conjunction with
// the this Utils library.
// ----------------------------------------------------

// ----------------------------------------------------
// In-source ViTest
// ----------------------------------------------------
if (import.meta.vitest) {
  const {describe, test, it, expect} =
        (import.meta.vitest)

  const { Sampler } =
        await import ('#src/Random/Sampler')
  const sampler = new Sampler(112961078)

  const { binBitXor : xor } = await import ('#src/Ops')

  describe('Mid vs xor', () => {
    it('(%d + %d) >> 1 == Xor', () => {
      const vals = [...Array(256)].map(
        () => [sampler.randRange(512),
               sampler.randRange(512)]
      )
      , mid = (a, b) => ((a + b) >> 1)
      , mids = vals.map(mid)
      , xors = vals.map(xor)
      expect(xors).not.toStrictEqual(mids)
    })
  })
  
}
// ----------------------------------------------------

