export default {
  Algo: {
    BinarySearch: await import ('./src/Algo/BinarySearch.js')
  },
  Ops: await import ('./src/Ops.js'),
  Random: {
    BoxMuller: await import('./src/Random/BoxMuller.js'),
    MersenneTwister: await import('./src/Random/MersenneTwister.js'),
    Sampler: await import('./src/Random/Sampler.js'),    
  },
  Utils: await import('./src/Utils.js')
}
